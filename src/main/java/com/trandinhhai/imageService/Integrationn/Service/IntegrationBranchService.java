package com.trandinhhai.imageService.Integrationn.Service;

import com.common.Response.CommonBranchDetailResponse;

public interface IntegrationBranchService {
    CommonBranchDetailResponse getDetailBranchByBranchId(String branchId);
}
