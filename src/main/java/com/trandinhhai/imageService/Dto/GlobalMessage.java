package com.trandinhhai.imageService.Dto;

import lombok.Data;

@Data
public class GlobalMessage {
    private String errorCode;
    private String message;
}
