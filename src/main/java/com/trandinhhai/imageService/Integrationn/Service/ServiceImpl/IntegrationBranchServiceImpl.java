package com.trandinhhai.imageService.Integrationn.Service.ServiceImpl;

import com.common.Model.CommonResponse;
import com.common.Request.CommonGetBranchDetailRequest;
import com.common.Response.CommonBranchDetailResponse;
import com.google.gson.Gson;
import com.trandinhhai.imageService.Common.Constant;
import com.trandinhhai.imageService.Integrationn.Service.IntegrationBranchService;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class IntegrationBranchServiceImpl implements IntegrationBranchService {

    private final QueryGateway queryGateway;

    public IntegrationBranchServiceImpl(QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @Override
    public CommonBranchDetailResponse getDetailBranchByBranchId(String branchId) {
        CommonBranchDetailResponse response = new CommonBranchDetailResponse();
        CommonResponse commonResponse = new CommonResponse();
        CommonGetBranchDetailRequest request = new CommonGetBranchDetailRequest();
        try {
            log.info("[START] {} getDetailBranchByBranchId with branchId = {}",
                    this.getClass().getSimpleName(), branchId);
            request.setBranchId(branchId);

            response = queryGateway.query(request, ResponseTypes.instanceOf(CommonBranchDetailResponse.class))
                    .join();
            log.info("[END] {} getDetailBranchByBranchId with respopnse = {} ",
                    this.getClass().getSimpleName(), new Gson().toJson(response));
        }catch (Exception e) {
            log.error("[ERROR] {} getDetailBranchByBranchId with error: ",
                    this.getClass().getSimpleName(), e);
            commonResponse.setErrorCode(Constant.errorCode.call_service_error);
            commonResponse.setMessage(Constant.errorMessage.CALL_SERVICE_ERROR);
            response.setCommonResponse(commonResponse);
        }
        return response;
    }
}
