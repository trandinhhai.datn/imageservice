package com.trandinhhai.imageService.Integrationn.Service;

import com.common.Response.CommonGetEmployeeDetailResponse;

public interface IntegrationHumanService {
    CommonGetEmployeeDetailResponse getDetailByUsername(String username);
}
