package com.trandinhhai.imageService.Integrationn.Service.ServiceImpl;

import com.common.Model.CommonResponse;
import com.common.Request.CommonGetEmployeeDetailRequest;
import com.common.Response.CommonGetEmployeeDetailResponse;
import com.google.gson.Gson;
import com.trandinhhai.imageService.Common.Constant;
import com.trandinhhai.imageService.Integrationn.Service.IntegrationHumanService;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class IntegrationHumanServiceImpl implements IntegrationHumanService {
    private final QueryGateway queryGateway;

    public IntegrationHumanServiceImpl(QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @Override
    public CommonGetEmployeeDetailResponse getDetailByUsername(String username) {
        CommonGetEmployeeDetailResponse response = new CommonGetEmployeeDetailResponse();
        CommonResponse commonResponse = new CommonResponse();
        CommonGetEmployeeDetailRequest request = new CommonGetEmployeeDetailRequest();
        try {
            log.info("[START] {} getDetailByUsername with username = {}",
                    this.getClass().getSimpleName(), username);
            request.setUsername(username);
            response = queryGateway.query(request, ResponseTypes.instanceOf(CommonGetEmployeeDetailResponse.class))
                    .join();
            log.info("[END] {} getDetailByUsername with response = {}",
                    this.getClass().getSimpleName(), new Gson().toJson(response));
        }catch (Exception e) {
            log.error("[ERROR] {} getDetailByUsername with error: ",
                    this.getClass().getSimpleName());
            commonResponse.setErrorCode(Constant.errorCode.call_service_error);
            commonResponse.setMessage(Constant.errorMessage.CALL_SERVICE_ERROR);
            response.setCommonResponse(commonResponse);
        }

        return response;
    }
}
