package com.trandinhhai.imageService.Common;

public interface Constant {

    interface errorCode {
        String success = "PROS-01-000";
        String server_error = "PROS-01-500";
        String call_service_error = "PROS-01-5001";
        String branch_id_empty = "PROS-01-001";
        String description_empty =  "PROS-01-002";
        String price_empty = "PROS-01-003";
        String category_id_empty = "PROS-01-004";

        String category_invalid = "PROS-01-005";
        String category_name_empty = "PROS-01-006";
        String product_id_empty = "PROS-01-007";
        String product_name_empty = "PROS-01-008";
        String category_name_exists = "PROS-01-009";
        String quantity_empty = "PROS-01-010";
        String branch_invalid = "PROS-01-011";
        String price_quantity_not_number = "PROS-01-012";
        String product_invalid = "PROS-01-013";

    }

    interface errorMessage {
        String SUCCESS = "success";
        String SERVER_ERROR = "Server error - Hệ thống đang gặp vấn đề";
        String CALL_SERVICE_ERROR = "Call service error - lỗi khi gọi sang hệ thống khác";
        String BRANCH_ID_EMPTY = "Branch id is empty - Mã chi nhánh bị trống";
        String DESCRIPTION_EMPTY = "Descrition is empty - Mô tả bị trống";
        String PRICE_EMPTY = "Amount is empty - Số tiền bị trống";
        String CATEGORY_ID_EMPTY = "category id is empty - Mã loại hàng bị trống";
        String CATEGORY_INVALID = "Category invalid - Loại hàng không hợp lệ";
        String CATEGORY_NAME_EMPTY = "Category name is empty - Tên loại hàng trống";
        String PRODUCT_ID_EMPTY = "Product id is empty - Mã sản phẩm trống";
        String PRODUCT_NAME_EMPTY = "Tên sản phẩm trống";
        String CATEGORY_NAME_EXISTS = "Tên loại sản phẩm đã tồn tại";
        String QUANTITY_EMPTY = "Số lượng bị trống";
        String BRANCH_INVALID = "Chi nhánh không hợp lệ";
        String PRICE_QUANTITY_NOT_NUMBER = "Giá hoặc số lượng không đúng";

        String PRODUCT_INVALID = "Sản phẩm không hợp lệ";
    }

    interface HumanErrorCode {
        String success = "HMS-01-000";
    }
    interface BranchErrorCode {
        String success = "BRS-01-000";
    }

    interface branch {
        String prefixBranchName = "CN";
    }
    interface spend {
        String prefixSpendName = "SP";
    }

    interface category {
        String prefixCategory = "CT";
    }
    interface product {
        String prefixProduct = "PRO";
    }

    interface province {
        String prefixProvince = "PR";
    }

    interface status {
        String ACTIVE = "ACTIVE";
    }
}
