package com.trandinhhai.imageService.Common;

import com.trandinhhai.imageService.Dto.GlobalMessage;
import com.trandinhhai.imageService.Model.MicroserviceResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class Until {

    public static String removeAcent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        temp = pattern.matcher(temp).replaceAll("");
        return temp.replaceAll("đ", "d");
    }

    public static boolean isEmtyOrBlank(String str) {
        if (StringUtils.isEmpty(str) || StringUtils.isBlank(str)) {
            log.info("{} is emty or blank", str);
            return true;
        } else {
            log.info("{} is not emty or blank", str);
            return false;
        }
    }

    public static boolean isValidUsername(String str) {
        //hợp lệ thì true, 6 - 12 ký tự, không có khoảng trắng
        Pattern patternDate = Pattern.compile("[a-z0-9_-]{6,12}$");
        return patternDate.matcher(str).matches();
    }

    public static boolean isStringSpecial(String str) {
        // Định nghĩa biểu thức chính quy để kiểm tra ký tự đặc biệt
        String regex = ".*[^a-zA-Z0-9].*";

        // Tạo Pattern và Matcher
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        // Kiểm tra xem chuỗi có chứa ký tự đặc biệt hay không
        if (matcher.matches()) {
            log.info("{} chứa ký tự đặc biệt.", str);
            return true;
        } else {
            log.info("{} không chứa ký tự đặc biệt.", str);
            return false;
        }
    }

    public static boolean isNumberic(String str) {
        // Định nghĩa biểu thức chính quy để kiểm tra chuỗi có phải là số hay không
        String regex = "^[0-9]+$";

        // Tạo Pattern và Matcher
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        // Kiểm tra xem chuỗi có phù hợp với biểu thức chính quy hay không
        if (matcher.matches()) {
            log.info("{} là một số.", str);
            return true;
        } else {
            log.info("{} không phải là một số.", str);
            return false;
        }
    }


    public static boolean isPhoneNumber(String str) {
        // Định nghĩa biểu thức chính quy để kiểm tra đầu chuỗi có bắt đầu bằng "84", "+84" hoặc "0" hay không
        String regex = "^(\\+84)";
        String regex2 = "^84";
        String regex3 = "^0";

        // Tạo Pattern và Matcher
        Pattern pattern = Pattern.compile(regex);
        Pattern pattern2 = Pattern.compile(regex2);
        Pattern pattern3 = Pattern.compile(regex3);

        Matcher matcher = pattern.matcher(str);
        Matcher matcher2 = pattern2.matcher(str);
        Matcher matcher3 = pattern3.matcher(str);

        // Kiểm tra xem chuỗi có phù hợp với biểu thức chính quy hay không
        if (matcher.find()) {
            if (str.length() == 12) {
                log.info("{} bắt đầu bằng '+84' và độ dài bằng 12", str);
                return true;
            } else {
                log.info("{} bắt đầu bằng '+84' và độ dài không bằng 12", str);
                return false;
            }
        } else if (matcher2.find()) {
            if (str.length() == 11) {
                log.info("{} bắt đầu bằng '84' và độ dài bằng 11", str);
                return true;
            } else {
                log.info("{} bắt đầu bằng '84' và độ dài không bằng 11", str);
                return false;
            }
        } else if (matcher3.find()) {
            if (str.length() == 10) {
                log.info("{} bắt đầu bằng '0' và độ dài bằng 10", str);
                return true;
            } else {
                log.info("{} bắt đầu bằng '0' và độ dài không bằng 10", str);
                return false;
            }
        }
        else {
            log.info("{} không bắt đầu bằng '84', '+84', hoặc '0'.", str);
            return false;
        }
    }

    public static String randomFourNumber() {
        // Tạo một đối tượng Random
        Random random = new Random();

        // Sinh số ngẫu nhiên có 4 chữ số
        int min = 1000; // Giá trị nhỏ nhất có thể sinh ra là 1000 (có 4 chữ số)
        int max = 9999; // Giá trị lớn nhất có thể sinh ra là 9999 (có 4 chữ số)
        int randomNumber = random.nextInt(max - min + 1) + min;
        return String.valueOf(randomNumber);
    }


    public static MicroserviceResponseBody setResponseSuccess() {
        MicroserviceResponseBody responseBody = new MicroserviceResponseBody();
        GlobalMessage globalMessage = new GlobalMessage();
        globalMessage.setErrorCode(Constant.errorCode.success);
        globalMessage.setMessage(Constant.errorMessage.SUCCESS);
        responseBody.setGlobalMessage(globalMessage);
        return responseBody;
    }

    public static MicroserviceResponseBody setResponseServerError() {
        MicroserviceResponseBody responseBody = new MicroserviceResponseBody();
        GlobalMessage globalMessage = new GlobalMessage();
        globalMessage.setErrorCode(Constant.errorCode.server_error);
        globalMessage.setMessage(Constant.errorMessage.SERVER_ERROR);
        responseBody.setGlobalMessage(globalMessage);
        return responseBody;
    }

    public static MicroserviceResponseBody setCallServiceError() {
        MicroserviceResponseBody responseBody = new MicroserviceResponseBody();
        GlobalMessage globalMessage = new GlobalMessage();
        globalMessage.setMessage(Constant.errorMessage.CALL_SERVICE_ERROR);
        globalMessage.setErrorCode(Constant.errorCode.call_service_error);
        responseBody.setGlobalMessage(globalMessage);
        return responseBody;
    }

    public static Pageable validatePageAble(String pageSize, String pageNumber) {
        if (StringUtils.isEmpty(pageSize) || pageSize.equals("0")) {
            pageSize = "10";
        }
        if (StringUtils.isEmpty(pageNumber)) {
            pageNumber = "1";
        }
        Pageable pageRequest = PageRequest.of(Integer.parseInt(pageNumber) - 1, Integer.parseInt(pageSize));
        return pageRequest;
    }

    public static String dateString(Date date) {
        log.info(" dateString with date = {}",
                date);
        String formattedDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = sdf.format(date);
        return formattedDate;
    }
}
