package com.trandinhhai.imageService.Model;

import com.trandinhhai.imageService.Dto.GlobalMessage;
import lombok.Data;

@Data
public class MicroserviceResponseBody {
    private GlobalMessage globalMessage;
    private Object responseBody;
}
